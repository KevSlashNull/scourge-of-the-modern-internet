<?php

use App\ScourgeLoader;
use App\View;

require __DIR__ . '/../vendor/autoload.php';

$scourges = new ScourgeLoader;
[$scourge, $verb] = $scourges->random();

$view = new View('main', options: [
  'scourge' => $scourge,
  'verb' => $verb,
  'tweet_link' => 'https://twitter.com/intent/tweet?url=' . urlencode('https://scourge-of-the-modern-internet.com') . '&text=' . urlencode("$scourge $verb the scourge of the modern internet.") . '&via=KevSlashNull'
]);
$view->render();
