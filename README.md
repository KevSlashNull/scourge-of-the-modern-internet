# The Scourge of the Modern Internet

This repository holds some of the scourges<sup>1</sup> of the modern internet.

🔗 [scourge-of-the-modern-internet.com][link]

[link]: https://scourge-of-the-modern-internet.com/

<sup>1</sup> “Somebody or something that is perceived as an agent of
punishment, destruction, or severe criticism.” — [urban dictionary][ud]

[ud]: https://www.urbandictionary.com/define.php?term=Scourge

## Adding a new scourge

Have you discovered something worse than is already on this site? All scourges
are stored in [`data/scourges.yml`](./data/scourges.yml) in [YAML][yml] format.

Fork this repository and add your own. Please keep them so that you and your
boss can still laugh about them, even if they may slightly offend one or both
of you.

The file has two lists. The name of the list is the verb, either `is` or `are`.
It is used to generate the text seen on the page: `SCOURGE (is|are) the scourge
of the modern internet.`, where `SCOURGE` is an entry in one of the lists.

[yml]: https://en.wikipedia.org/wiki/YAML

## Running the page locally

If you have PHP 8.0 and [composer][composer] installed, run `composer install`
and then run `bin/scourge`. Now you can go to <http://127.0.0.1:8000> and find
a local version of the page.

[composer]: https://getcomposer.org/