<?php

namespace App;

class View
{
  private string $file;

  public function __construct(string $file, public array $options = [])
  {
    $file = str_replace(['.', '/'], ['/', ''], $file);
    $this->file = __DIR__ . "/../views/$file.php";
  }

  public function render()
  {
    extract($this->options);
    require($this->file);
  }
}
