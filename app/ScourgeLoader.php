<?php

namespace App;

use Spyc;

class ScourgeLoader
{
  private array $scourgesIs, $scourgesAre;

  public function __construct()
  {
    [$this->scourgesIs, $this->scourgesAre] = $this->load();
  }

  public function random()
  {
    [$verb, $array] = $this->getVerbAndArray();
    $random_key = array_rand($array);
    $value = $array[$random_key];

    return [$value, $verb];
  }

  private function getVerbAndArray()
  {
    $verb = random_int(0, 1) ? 'is' : 'are';
    $array = $verb === 'is' ? $this->scourgesIs : $this->scourgesAre;

    return [$verb, $array];
  }

  private function load(): array
  {
    $file = Spyc::YAMLLoad(__DIR__ . '/../data/scourges.yml');

    return [$file['is'], $file['are']];
  }
}
