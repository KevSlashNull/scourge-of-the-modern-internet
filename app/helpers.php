<?php

if (!function_exists('e')) {
  function e($string): string
  {
    return htmlspecialchars($string ?? '', ENT_QUOTES, 'UTF-8', double_encode: true);
  }
}
