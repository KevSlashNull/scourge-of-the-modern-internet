<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>The Scourge of the Modern Internet</title>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Merriweather+Sans:wght@400;800&family=Source+Sans+Pro:wght@500&display=swap" rel="stylesheet">

  <meta property="og:title" content="The Scourge of the Modern Internet" />
  <meta property="og:description" content="A mockery of the annoyances that the “modern internet” causes all of us." />
  <meta property="og:image" content="https://scourge-of-the-modern-internet.com/og-image.png" />
  <meta property="og:image:width" content="1920" />
  <meta property="og:image:height" content="1080" />
  <meta name="twitter:card" content="summary_large_image" />
  <meta name="twitter:creator" content="@KevSlashNull" />
  <meta name="twitter:title" content="The Scourge of the Modern Internet">
  <meta name="twitter:description" content="A mockery of the annoyances that the “modern internet” causes all of us.">
  <meta name="twitter:image" content="https://scourge-of-the-modern-internet.com/og-image.png">
  <style>
    body,
    html {
      height: 100%;
      width: 100%;
      font-family: 'Merriweather Sans';
      padding: 0;
      margin: 0;
      font-size: 18px;
      background: black;
      color: white;
    }

    .container {
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
    }

    span {
      width: 100%;
    }

    .scourge {
      font-size: 4rem;
      margin-bottom: 1rem;
      font-weight: bold;
      text-align: center;
      height: 50%;
      display: flex;
      align-items: flex-end;
      justify-content: center;
    }

    .what {
      text-align: center;
      font-size: 3rem;
      font-weight: bold;
      height: 50%;
    }

    .what a {
      display: inline-block;
      margin-top: 2rem;
      font-size: 1.5rem;
      font-weight: normal;
    }

    .made-by {
      position: fixed;
      bottom: 8px;
      text-align: center;
      width: 100%;
      font-family: 'Source Sans Pro', sans-serif;
      font-size: 18px;
      color: #dfdfdf;
    }

    a {
      color: inherit !important;
    }

    @media (max-width: 800px) {

      body,
      html {
        font-size: 12px;
      }
    }
  </style>
</head>

<body>
  <main class="container">
    <span class="scourge"><?= e($scourge) ?></span>
    <span class="what">
      <?= e($verb) ?> the scourge of the modern internet.
      <br>
      <a href="<?= $tweet_link ?>" target="_blank">Tweet this</a>
    </span>
  </main>
  <footer class="made-by">
    by <a href="https://twitter.com/KevSlashNull">Kev</a> &amp; friends &mdash; <a href="https://gitlab.com/kevslashnull/scourge-of-the-modern-internet">Source</a>
  </footer>
</body>

</html>